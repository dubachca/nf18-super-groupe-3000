import psycopg2

HOST = "tuxa.sme.utc"
USER = "bdd0p034"
PASSWORD = "3NfUPjcm"
DATABASE = "dbbdd0p034"

#connexion à la base de données

conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
cur = conn.cursor()

def add_medicament():
    try:
        molecule=input("Entrez le nom de la molécule:")
        description=input("Entrez la description de la molécule:")
        sql="insert into medicament values(%s,%s)"
        cur.execute(sql,(molecule,description))
        conn.commit()
        print("add 1 row")
    except psycopg2.errors.InvalidDatetimeFormat as e1:
        conn.rollback()
        print("Error:",e1) 


def delete_medicament():
    molecule=input("Entrez le nom de la molécule à supprimer:")
    sql_count=("select count(*) from medicament where molecule = %s")
    cur.execute(sql_count,(molecule,))
    res_count = cur.fetchall()
    if (res_count[0]==0):
        print('Molecule inexistante')
    else:
        sql = "delete from medicament where molecule = %s"
        cur.execute(sql,(molecule,))
        conn.commit()
        print('row deleted')

def show_medicament(row):
    print("----------------------------")
    print("molecule: %s\ndescription: %s"%(row[0],row[1]))

def show_all_medicaments():
    sql = 'select * from medicament'
    cur.execute(sql)
    res = cur.fetchall()
    for row in res:
        show_medicament(row)
        
def modifier_medicament():
    molecule=input("Entrez le nom de la molécule à modifier:")
    column = 0
    while(column!='1' and column!='2'):
        column=input("Quel attribut voulez vous modifier?\n1.molecule\n2.description\n")
    def f(x):
            return {
                '1':'molecule',
                '2':'description',
            }[column] 
    column=f(column)
    valeur=input("Entrez la nouvelle valeur:")
    try:
        sql = "Update medicament set %s = '%s' where molecule='%s'"%(column,valeur,molecule)
        cur.execute(sql)
        conn.commit()
        print("value updated")
    except psycopg2.errors.InvalidDatetimeFormat as e1:
        conn.rollback()
        print("Error:",e1) 
       
conn.close()



    
    
