drop table if exists mesure_taille;
drop table if exists mesure_poids;
drop table if exists observation;
drop table if exists description_procedure;
drop table if exists resultat;
drop table if exists poids;
drop table if exists taille;
drop table if exists dossier;

create table dossier (
	id serial primary key
);

create table taille (
	taille integer primary key
);

create table poids (
	poids integer primary key
);

create table resultat (
	id serial primary key,
	resultat_url varchar not null,
	dat Date not null,
	tim Time not null,
	idDossier integer references Dossier(id) not null
);

create table description_procedure (
	id serial primary key,
	description_procedure varchar not null,
	dat Date not null,
	tim Time not null,
	idDossier integer references Dossier(id) not null
);

create table observation (
	id serial primary key,
	observation varchar not null,
	dat Date not null,
	tim Time not null,
	idDossier integer references Dossier(id) not null
);

create table mesure_poids (
	poids integer references Poids(poids),
	idDossier integer references Dossier(id),
	dat Date not null,
	tim Time not null,
	primary key(poids, idDossier)
);

create table mesure_taille (
	taille integer references taille(taille),
	idDossier integer references dossier(id),
	dat Date not null,
	tim Time not null,
	primary key(taille, idDossier)
);
