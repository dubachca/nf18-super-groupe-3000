DROP TABLE IF EXISTS Client CASCADE;
DROP TABLE IF EXISTS Espece CASCADE;
DROP TABLE IF EXISTS Personnel CASCADE;
DROP TABLE IF EXISTS Patient CASCADE;
DROP TABLE IF EXISTS Possession CASCADE;
DROP TABLE IF EXISTS Responsable CASCADE;
DROP TABLE IF EXISTS Medicament CASCADE;
DROP TABLE IF EXISTS Autorisation CASCADE;
DROP TABLE IF EXISTS Prescription CASCADE;
DROP TABLE IF EXISTS description_procedure CASCADE;
drop table if exists mesure_taille;
drop table if exists mesure_poids;
drop table if exists observation;
drop table if exists description_procedure;
drop table if exists resultat;
drop table if exists poids;
drop table if exists taille;
drop table if exists dossier;



CREATE TABLE IF NOT EXISTS Client(
    id_client serial,
    nom varchar not null,
    prenom varchar not null,
    date_nai date not null,
    adresse varchar not null,
    tel_client varchar not null,
    primary key (id_client)
);

CREATE TABLE IF NOT EXISTS Espece(
    nom varchar(20) PRIMARY KEY
);

create table dossier (
	id serial primary key
);

CREATE TABLE IF NOT EXISTS Personnel(
    id_personnel serial,
    nom varchar not null,
    prenom varchar not null,
    date_nai date not null,
    adresse varchar not null,
    tel_Personnel varchar not null,
    poste varchar not null check (poste in ('veterinaire', 'assistant')),
    specialite varchar references espece(nom),
    primary key (id_personnel)
);


CREATE TABLE IF NOT EXISTS Patient(
    id_patient serial PRIMARY KEY,  
    nom varchar(20) NOT NULL, 
    date_naissance date, 
    numero_puce varchar UNIQUE, 
    numero_passeport varchar UNIQUE, 
    date_arrive date NOT NULL, 
    date_depart date, 
    espece varchar(20) NOT NULL,
    FOREIGN KEY(espece) REFERENCES Espece(nom)
);



CREATE TABLE IF NOT EXISTS Possession(
    id_client serial,
    id_patient serial,
    debut_possession date NOT NULL,
    fin_possession date NOT NULL,
    FOREIGN KEY (id_client) REFERENCES Client(id_client) ON DELETE CASCADE,
    FOREIGN KEY (id_patient) REFERENCES Patient(id_patient) ON DELETE CASCADE,
    PRIMARY KEY(id_client, id_patient) 
);

CREATE TABLE IF NOT EXISTS Responsable(
    id_patient serial,
    id_personnel serial,
    date_intervention date NOT NULL, 
    FOREIGN KEY (id_patient) REFERENCES Patient(id_patient) ON DELETE CASCADE,
    FOREIGN KEY (id_personnel) REFERENCES Personnel(id_personnel) ON DELETE CASCADE,
    PRIMARY KEY (id_patient, id_personnel) 
);

CREATE TABLE IF NOT EXISTS Medicament(
    molecule VARCHAR PRIMARY KEY,
    description VARCHAR NOT NULL
);


CREATE TABLE IF NOT EXISTS Autorisation(
    molecule VARCHAR REFERENCES Medicament(molecule),
    espece VARCHAR(20) REFERENCES Espece(nom),
    PRIMARY KEY (molecule,espece)
);

CREATE TABLE IF NOT EXISTS Prescription(
    id_dossier INTEGER REFERENCES Dossier(id),
    molecule VARCHAR REFERENCES Medicament(molecule),
    date_debut DATE NOT NULL,
    duree INTEGER NOT NULL,
    quantite INTEGER NOT NULL,
    PRIMARY KEY (id_dossier,molecule,date_debut)
);

create table IF NOT EXISTS description_procedure(
	id serial primary key,
	description_procedure varchar not null,
	dat Date not null,
	ti Time not null,
	tim Time not null,
	idDossier integer references Dossier(id) not null
);




create table taille (
	taille integer primary key
);

create table poids (
	poids integer primary key
);

create table resultat (
	id serial primary key,
	resultat_url varchar not null,
	dat Date not null,
	tim Time not null,
	idDossier integer references Dossier(id) not null
);


create table observation (
	id serial primary key,
	observation varchar not null,
	dat Date not null,
	tim Time not null,
	idDossier integer references Dossier(id) not null
);

create table mesure_poids (
	poids integer references Poids(poids),
	idDossier integer references Dossier(id),
	dat Date not null,
	tim Time not null,
	primary key(poids, idDossier)
);

create table mesure_taille (
	taille integer references taille(taille),
	idDossier integer references dossier(id),
	dat Date not null,
	tim Time not null,
	primary key(taille, idDossier)
);
