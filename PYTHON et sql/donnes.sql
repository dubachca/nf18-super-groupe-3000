INSERT INTO ESPECE VALUES ('felin');
INSERT INTO ESPECE VALUES ('canide');
INSERT INTO ESPECE VALUES ('reptile');
INSERT INTO ESPECE VALUES ('rongeur');
INSERT INTO ESPECE VALUES ('oiseaux');
INSERT INTO ESPECE VALUES ('autre');

INSERT INTO Client VALUES (DEFAULT,'Emmanuel','Macron','1997-12-21','55 rue Faubourg-Saint-Honoré Paris','01 42 92 81 00');
INSERT INTO Client VALUES(DEFAULT,'Brigitte','Macron','1953-04-13','55 rue Faubourg-Saint-Honoré Paris','01 52 23 84 02');
INSERT INTO Client VALUES(DEFAULT,'Kylian','Mbappé','1998-12-20','24 RUE DU CDT GUILBAUD - 75016 PARIS','02 23 96 87 22');
INSERT INTO Client VALUES(DEFAULT,'Olivier','Véran','1980-04-22','14, avenue Duquesne 75007 Paris','05 03 70 70 41');

INSERT INTO Personnel VALUES(DEFAULT,'Perez','Nadine','1955-07-21','P.O. Box 660, 7436 Commodo Street','01 35 97 09 07','veterinaire','felin');
INSERT INTO Personnel VALUES(DEFAULT,'Frye','Aristotle','1978-03-16','P.O. Box 345, 5461 Vel Rd.','07 51 74 17 30','veterinaire','reptile');
INSERT INTO Personnel VALUES(DEFAULT,'Perkins','Amity','1975-10-16','7432 Ipsum St.','06 28 78 32 82','veterinaire','rongeur');
INSERT INTO Personnel VALUES(DEFAULT,'Delgado','Jael','1966-06-17','Ap #382-1700 Etiam Av.','03 84 45 31 33','veterinaire','rongeur');
INSERT INTO Personnel VALUES(DEFAULT,'Christensen','Arthur','1993-07-20','4805 Amet Avenue','08 98 77 54 59','assistant','felin');
INSERT INTO Personnel VALUES(DEFAULT,'Stout','Dorian','1991-08-12','700-4275 Faucibus Rd.','06 92 99 96 55','assistant','canide');
INSERT INTO Personnel VALUES(DEFAULT,'Peterson','Alisa','1993-04-12','14, avenue Duquesne 75007 Paris','05 03 70 70 41','assistant','reptile');

INSERT INTO Patient VALUES(DEFAULT,'fel1',to_date('2012','yyyy'),'fl9210','pass9213','2019-04-02','2019-05-01','felin');
INSERT INTO Patient VALUES(DEFAULT,'fel2',to_date('2019','yyyy'),'fl1312','pass2443','2019-09-04','2019-10-12','felin');
INSERT INTO Patient VALUES(DEFAULT,'rep1',to_date('2018','yyyy'),'re4123','pass4019','2018-12-14','2019-01-05','reptile');
INSERT INTO Patient VALUES(DEFAULT,'rep2',to_date('2019','yyyy'),'re5342','pass5203','2019-03-30','2019-05-15','reptile');
INSERT INTO Patient VALUES(DEFAULT,'can1',to_date('2020','yyyy'),'ca5272','pass9444','2019-12-15','2020-01-08','canide');
INSERT INTO Patient VALUES(DEFAULT,'ron1',to_date('2018','yyyy'),'ro1713','pass0049','2020-02-13',NULL,'rongeur');
INSERT INTO Patient VALUES(DEFAULT,'ois1',to_date('2019','yyyy'),'oi0423','pass2923','2018-12-14','2019-01-05','reptile');

INSERT INTO Responsable VALUES(1,1,'2019-04-12');
INSERT INTO Responsable VALUES(2,1,'2019-10-01');
INSERT INTO Responsable VALUES(3,2,'2019-01-12');
INSERT INTO Responsable VALUES(4,2,'2019-04-08');
INSERT INTO Responsable VALUES(5,3,'2020-02-16');
INSERT INTO Responsable VALUES(6,2,'2020-01-13');
INSERT INTO Responsable VALUES(6,6,'2019-01-12');

INSERT INTO Possession VALUES(1,1,to_date('2012','yyyy'),to_date('2015','yyyy'));
INSERT INTO Possession VALUES(2,1,to_date('2015','yyyy'),to_date('2020','yyyy'));
INSERT INTO Possession VALUES(2,2,to_date('2013','yyyy'),to_date('2020','yyyy'));
INSERT INTO Possession VALUES(2,3,to_date('2012','yyyy'),to_date('2018','yyyy'));
INSERT INTO Possession VALUES(3,4,to_date('2014','yyyy'),to_date('2020','yyyy'));
INSERT INTO Possession VALUES(3,5,to_date('2016','yyyy'),to_date('2020','yyyy'));
INSERT INTO Possession VALUES(4,6,to_date('2016','yyyy'),to_date('2020','yyyy'));



