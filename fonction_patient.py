import sys
import psycopg2

HOST = "localhost"
USER = "Simon"
PASSWORD = "crc7u937"
DATABASE = "mydb"

conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
cur = conn.cursor()

def add_patient():
    try:
        nom = input("Entrez le nom du patient:")
        naissance = input("Entrez la date de naissance, par exemple 2020-04-02")
        puce = input("Entrez le numero de puce d'identification:")
        passeport = input("Entrez le numero de passeport:")
        arrive = input("Entrez la date d'arrivé, par exemple 2020-04-02")
        depart = input("Entrez la date de départ, par exemple 2020-04-02")
        
        espece = 0
        while(espece!='1'and espece!='2'and espece!='3'and espece!='4'and espece!='5'and espece!='6'):
            espece = input("Choisir son espece: \n1.felin, 2.canidé, 3.reptile, 4.rongeur, 5.oiseau, 6.autre")      

        def f(x):
            return {
                '1':'felin',
                '2':'canide',
                '3':'reptile',
                '4':'rongeur',
                '5':'oiseau',
                '6':'autre',
            }[espece]
        
        espece_nom = f(espece)

        sql = "insert into patient values(default,'%s','%s','%s','%s','%s','%s','%s')"%(nom,naissance,puce,passeport,arrive,depart,espece_nom)
        cur.execute(sql)
        conn.commit()
        print("add 1 row")
    except psycopg2.errors.InvalidDatetimeFormat as e1:
        conn.rollback()
        print("Error:",e1)
        print("Veuillez réessayer!") 
        enter = input("Appuyez sur 'enter' pour continuer")

def delete_patient():
    show_name()
    id_patient = input("Entrez l'id du patient que vous voulez supprimer, entrez 'q' pour annuler:")
    if(id_patient=='q'):
        return
    sql_count = "select count(*) from patient where id_patient = %s"%(id_patient)
    cur.execute(sql_count)
    res_count = cur.fetchall()
    if (res_count[0][0]==0):
        print('Patient inexistant')
        enter = input("Appuyez sur 'enter' pour continuer")
    else:
        sql = "delete from patient where id_patient = %s"%(id_patient)
        cur.execute(sql)
        conn.commit()

def show_patients(name):
    sql = 'select * from Patient where id_patient = %s'%(name)
    cur.execute(sql)
    tab = cur.fetchone()
    print("----------------------------")
    print("id: %s\nnom: %s\ndate de naissance:%s\nnumero puce:%s\nnumero passeport:%s\ndate d'arrive:%s\ndate de depart:%s\nespece:%s\n"%(tab[0],tab[1],tab[2],tab[3],tab[4],tab[5],tab[6],tab[7]))

    choix = 0
    while (choix!='1' and choix!='2' and choix!='3'):
        print("1.Afficher la liste de ses propriétaires\n2.Afficher la liste des vétérinaire qui l'ont suivi\n3.Précédent")
        choix = input("Entrez votre choix:")
    if(choix=='1'):
        sql = "select c.nom, p.id_patient, p.debut_possession, p.fin_possession from Possession p left outer join Client c on p.id_client = c.id_client where id_patient = %s"%(name)
        cur.execute(sql)
        res = cur.fetchall()
        for row in res:
            print("Le client %s possède ce patient depuis %s à %s"%(row[0],row[2],row[3]))
        
        enter= input("Appuyez sur 'enter' pour continuer...")

    elif (choix=='2'):
        sql="select p.nom, r.date_intervention from responsable r left outer join personnel p on p.id_personnel = r.id_personnel where id_patient=%s"%(name)
        cur.execute(sql)
        res = cur.fetchall()
        print("----------------------------")
        for row in res:
            print("%s a suivi ce patient le %s"%(row[0],row[1]))
        enter= input("Appuyez sur 'enter' pour continuer...")

    elif(choix=='3'):
        pass
    


def show_name():
    sql = 'select id_patient, nom from patient'
    cur.execute(sql)
    res = cur.fetchall()
    for row in res:
        print('%s: %s'%(row[0],row[1]))

def modifier_patient():
    id_patient = input("Entrez l'id de patient: ")

    column = 0
    while(column!='1' and column!='2' and column!='3' and column!='4' and column!='5' and column!='6' and column!='7' and column!='8'):
        column = input("Quel attribut voulez vous modifier?\n1.nom\n2.date de naissance\n3.numero puce\n4.numero passeport\n5.date d'arrive\n6.date de depart\n7.espece\n8.annuler\n")
    

    def f(x):
            return {
                '1':'nom',
                '2':'date_naissance',
                '3':'numero_puce',
                '4':'numero_passeport',
                '5':'date_arrive',
                '6':'date_depart',
            }[column]  
    column = f(column)
    valeur = input("Entrez la nouvelle valeur:")
    try:
        sql = "Update patient set %s = '%s' where id_patient=%s"%(column, valeur, id_patient)
        cur.execute(sql)
        conn.commit()
    except psycopg2.errors.InvalidDatetimeFormat as e1:
        conn.rollback()
        print("Error:",e1) 






