DROP TABLE IF EXISTS Espece CASCADE;
DROP TABLE IF EXISTS Dossier CASCADE;
DROP TABLE IF EXISTS Medicament CASCADE;
DROP TABLE IF EXISTS Autorisation CASCADE;
DROP TABLE IF EXISTS Prescription CASCADE;

CREATE TABLE IF NOT EXISTS Espece(
    nom VARCHAR(20) CHECK (nom in ('felin','canide','reptile','rongeur','oiseaux')) PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS Dossier(
    id INTEGER PRIMARY KEY
);


CREATE TABLE IF NOT EXISTS Medicament(
    molecule VARCHAR PRIMARY KEY,
    description VARCHAR NOT NULL
);

CREATE TABLE IF NOT EXISTS Autorisation(
    molecule VARCHAR REFERENCES Medicament(molecule),
    espece VARCHAR(20) REFERENCES Espece(nom),
    PRIMARY KEY (molecule,espece)
);

CREATE TABLE IF NOT EXISTS Prescription(
    id_dossier INTEGER REFERENCES Dossier(id),
    molecule VARCHAR REFERENCES Medicament(molecule),
    date_debut DATE NOT NULL,
    duree INTEGER NOT NULL,
    quantite INTEGER NOT NULL,
    PRIMARY KEY (id_dossier,molecule,date_debut)
);
