#!/usr/bin/python3

import psycopg2
import sys
from datetime import date
from datetime import datetime

HOST = ""
USER = ""
PASSWORD = ""
DATABASE = ""


# Retour 0 == opération reussie ; retour == 1 operation ratee

def insert_taille ( taille:int ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0;

	sql = "insert into taille (taille) values (" + str(taille) + ");"

	try :
		res.execute(sql)
		conn.commit()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1

	conn.close()
	return ret;


def insert_poids ( poids:int ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "insert into poids (poids) values (" + str(poids) + ");"

	try :
		res.execute(sql)
		conn.commit()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1
	
	conn.close()
	return ret

# This insert returns the row inserted
def insert_dossier ( ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "insert into dossier default values;"

	try :
		res.execute(sql)
		conn.commit()
		ret = res.fetchone();
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1

	conn.close()
	return ret

def insert_description_procedure ( description_procedure:str, idDossier:int ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "insert into description_procedure (description_procedure, dat, tim, idDossier) values (" \
		+ "'" + description_procedure + "', " \
		+ "'" + str(date.today()) + "', "\
		+ "'" + str(datetime.now().strftime("%H:%M:%S")) + "', "\
		+ str(idDossier) \
		+ ");"

	try :
		res.execute(sql)
		conn.commit()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1

	conn.close()
	return ret

def insert_observation ( observation:str, idDossier:int ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "insert into observation (observation, dat, tim, idDossier) values (" \
		+ "'" + observation + "', " \
		+ "'" + str(date.today()) + "', "\
		+ "'" + str(datetime.now().strftime("%H:%M:%S")) + "', "\
		+ str(idDossier) \
		+ ");" \

	try :
		res.execute(sql)
		conn.commit()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1

	conn.close()
	return ret

def insert_mesure_taille ( taille:int, idDossier:int ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "insert into mesure_taille (taille, idDossier, dat, tim) values (" \
		+ str(taille) + ", " \
		+ str(idDossier)  + ", "\
		+ "'" + str(date.today()) + "', "\
		+ "'" + str(datetime.now().strftime("%H:%M:%S")) + "'" \
		+ ");" \

	try :
		res.execute(sql)
		conn.commit()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret =1

	conn.close()
	return ret

def insert_mesure_poids ( poids:int, idDossier:int ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "insert into mesure_poids (poids, idDossier, dat, tim) values (" \
		+ str(poids) + ", " \
		+ str(idDossier)  + ", "\
		+ "'" + str(date.today()) + "', "\
		+ "'" + str(datetime.now().strftime("%H:%M:%S")) + "'"\
		+ ");" \

	try :
		res.execute(sql)
		conn.commit()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1

	conn.close()
	return ret

def insert_resultat ( resultat_url:str, idDossier:int ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "insert into resultat (resultat_url, dat, tim, idDossier) values (" \
		+ "'" + resultat_url + "', " \
		+ "'" + str(date.today()) + "', "\
		+ "'" + str(datetime.now().strftime("%H:%M:%S")) + "', "\
		+ str(idDossier) \
		+ ");"

	try :
		res.execute(sql)
		conn.commit()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1

	conn.close()
	return ret