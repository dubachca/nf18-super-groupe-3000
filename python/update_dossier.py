#!/usr/bin/python3

import psycopg2
import sys
from datetime import date
from datetime import datetime

HOST = ""
USER = ""
PASSWORD = ""
DATABASE = ""


# Retour 0 == opération reussie ; retour == 1 operation ratee

def update_description_procedure ( description_procedure:str, idDossier:int, date_enregistrement:str ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "update description_procedure set " \
		+ "description_procedure = '" + description_procedure + "', "  \
		+ "dat = '" + str(date.today()) +"', " \
		+ "tim = '" + str(datetime.now().strftime("%H:%M:%S")) + "' " \
		+ "where idDossier = "+ str(idDossier) +" and dat = '" + date_enregistrement +"' ;"

	try :
		res.execute(sql)
		conn.commit()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1

	conn.close()
	return ret

def update_observation ( observation:str, idDossier:int, date_enregistrement:str ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "update observation set " \
		+ "observation = '" + observation + "', "  \
		+ "dat = '" + str(date.today()) +"', " \
		+ "tim = '" + str(datetime.now().strftime("%H:%M:%S")) + "' " \
		+ "where idDossier = "+ str(idDossier) +" and dat = '" + date_enregistrement +"' ;"

	try :
		res.execute(sql)
		conn.commit()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1

	conn.close()
	return ret

def update_mesure_taille ( taille:int, idDossier:int, date_enregistrement:str ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "update mesure_taille set " \
		+ "taille = " + str(taille) + ", "  \
		+ "dat = '" + str(date.today()) +"', " \
		+ "tim = '" + str(datetime.now().strftime("%H:%M:%S")) + "' " \
		+ "where idDossier = "+ str(idDossier) +" and dat = '" + date_enregistrement +"' ;"

	try :
		res.execute(sql)
		conn.commit()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret =1

	conn.close()
	return ret

def update_mesure_poids ( poids:int, idDossier:int, date_enregistrement:str ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "update mesure_poids set " \
		+ "poids = " + str(poids) + ", "  \
		+ "dat = '" + str(date.today()) +"', " \
		+ "tim = '" + str(datetime.now().strftime("%H:%M:%S")) + "' " \
		+ "where idDossier = " + str(idDossier) + " and dat = '" + date_enregistrement + "' ;" \

	try :
		res.execute(sql)
		conn.commit()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1

	conn.close()
	return ret

def update_resultat ( resultat_url:str, idDossier:int, date_enregistrement:str ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "update resultat set " \
		+ "resultat_url = '" + resultat_url + "', "  \
		+ "dat = '" + str(date.today()) +"', " \
		+ "tim = '" + str(datetime.now().strftime("%H:%M:%S")) + "' " \
		+ "where idDossier = "+ str(idDossier) +" and dat = '" + date_enregistrement +"' ;"

	try :
		res.execute(sql)
		conn.commit()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1

	conn.close()
	return ret
