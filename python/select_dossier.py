#!/usr/bin/python3

import psycopg2
import sys
from datetime import date
from datetime import datetime

HOST = ""
USER = ""
PASSWORD = ""
DATABASE = ""

# Retour row si reussi ; retour == 1 operation ratee

def select_all_taille () :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0;

	sql = "select * from taille;"

	try :
		res.execute(sql)
		conn.commit()
		ret = res.fetchall()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1

	conn.close()
	return ret;

def select_taille ( taille:int ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0;

	sql = "select * from taille where taille = "+ str(taille) +";"

	try :
		res.execute(sql)
		conn.commit()
		ret = res.fetchone()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1

	conn.close()
	return ret;

def select_all_poids () :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "select * from poids;"

	try :
		res.execute(sql)
		conn.commit()
		ret = res.fetchall()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1
	
	conn.close()
	return ret

def select_poids ( poids:int ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "select * from poids where poids = " + str(poids) + ";"

	try :
		res.execute(sql)
		conn.commit()
		ret = res.fetchone()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1
	
	conn.close()
	return ret

def select_all_dossier () :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "select * from dossier;"

	try :
		res.execute(sql)
		conn.commit()
		ret = res.fetchall();
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1

	conn.close()
	return ret

def select_dossier ( idDossier:int ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "select * from dossier where id = " + str(idDossier) + ";"

	try :
		res.execute(sql)
		conn.commit()
		ret = res.fetchone();
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1

	conn.close()
	return ret

def select_description_procedure_by_dossierId_and_date ( idDossier:int, date_target:str ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "select * from description_procedure where idDossier = " + str(idDossier) +" and dat = "+ date_target + ";"

	try :
		res.execute(sql)
		conn.commit()
		ret = res.fetchall()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1

	conn.close()
	return ret

def select_description_procedure_by_dossierId ( idDossier:int ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "select * from description_procedure where idDossier = " + str(idDossier) +";"

	try :
		res.execute(sql)
		conn.commit()
		ret = res.fetchall()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1

	conn.close()
	return ret

def select_observation_by_dossierId ( idDossier:int ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "select * from observation where idDossier = " + str(idDossier) +";"

	try :
		res.execute(sql)
		conn.commit()
		ret = res.fetchall()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1

	conn.close()
	return ret

def select_observation_by_dossierId_and_date ( idDossier:int, date_target:str ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "select * from observation where idDossier = " + str(idDossier) +" and dat = "+ date_target + ";"

	try :
		res.execute(sql)
		conn.commit()
		ret = res.fetchall()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1

	conn.close()
	return ret

def select_mesure_taille_by_dossierId ( idDossier:int ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "select * from mesure_taille where idDossier = " + str(idDossier) +";"

	try :
		res.execute(sql)
		conn.commit()
		ret = res.fetchall()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret =1

	conn.close()
	return ret

def select_mesure_taille_by_dossierId_and_date ( idDossier:int, date_target:str ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "select * from mesure_taille where idDossier = " + str(idDossier) +" and dat = "+ date_target + ";"

	try :
		res.execute(sql)
		conn.commit()
		ret = res.fetchall()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret =1

	conn.close()
	return ret

def select_mesure_poids_by_dossierId_and_date ( idDossier:int, date_target:str ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "select * from poids where idDossier = " + str(idDossier) +" and dat = "+ date_target + ";"

	try :
		res.execute(sql)
		conn.commit()
		ret = res.fetchall()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1

	conn.close()
	return ret

def select_mesure_poids_by_dossierId ( idDossier:int ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "select * from poids where idDossier = " + str(idDossier) +";"

	try :
		res.execute(sql)
		conn.commit()
		ret = res.fetchall()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1

	conn.close()
	return ret

def select_resultat_by_dossierId ( idDossier:int ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "select * from resultat where idDossier = " + str(idDossier) +";"

	try :
		res.execute(sql)
		conn.commit()
		ret = res.fetchall()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1

	conn.close()
	return ret

def select_resultat_by_dossierId_and_date ( idDossier:int, date_target:str ) :
	conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
	res = conn.cursor()

	ret = 0

	sql = "select * from resultat where idDossier = " + str(idDossier) +" and dat = "+ date_target + ";"

	try :
		res.execute(sql)
		conn.commit()
		ret = res.fetchall()
	except :
		print("Unexpected error:", sys.exc_info()[0])
		ret = 1

	conn.close()
	return ret